# PyTorch-Speaker-Verification

Requirements:
* Python 3.5+
* PyTorch 0.4.1
* numpy 1.15.4
* librosa 0.6.1

Usage:
* see main.py and speechrecognition.py