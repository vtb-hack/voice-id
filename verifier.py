import os
import random
import time
import torch
import pickle
from torch.utils.data import DataLoader
import torch.nn.functional as F
from data_preprocess import process_input

from data_load import SpeakerDatasetPredictPreprocessed
from speech_embedder_net import SpeechEmbedder, GE2ELoss, get_centroids, get_cossim

def predict(path, centroids_path='enrollment_centroids.pkl'):
    process_input(path)

    predict_dataset = SpeakerDatasetPredictPreprocessed()

    test_loader = DataLoader(predict_dataset, batch_size=1, num_workers=8, drop_last=True)

    embedder_net = SpeechEmbedder()
    embedder_net.load_state_dict(torch.load('./mdl.model'))
    embedder_net.eval()

    with open(centroids_path, 'rb') as f:
        enrollment_centroids = pickle.load(f)

    batch_avg_EER = 0
    for batch_id, mel_db_batch in enumerate(test_loader):
        '''
        #For storing enrollment
        enrollment_batch = torch.reshape(mel_db_batch, (5, mel_db_batch.size(2), mel_db_batch.size(3)))
        enrollment_embeddings = embedder_net(enrollment_batch)
        enrollment_embeddings = torch.reshape(enrollment_embeddings, (1, 5, enrollment_embeddings.size(1)))
        enrollment_centroids = get_centroids(enrollment_embeddings)

        with open('enrollment_centroids.pkl', 'wb') as f:
            pickle.dump(enrollment_centroids, f)

        '''
        verification_batch = torch.split(mel_db_batch, int(mel_db_batch.size(1)//mel_db_batch.size(1)), dim=1)
        verification_batch = verification_batch[0]
        verification_batch = torch.reshape(verification_batch, (1, verification_batch.size(2), verification_batch.size(3)))

        perm = random.sample(range(0,verification_batch.size(0)), verification_batch.size(0))
        unperm = list(perm)
        for i,j in enumerate(perm):
            unperm[j] = i

        verification_batch = verification_batch[perm]
        verification_embeddings = embedder_net(verification_batch)
        verification_embeddings = verification_embeddings[unperm]

        verification_embeddings = torch.reshape(verification_embeddings, (1, 1, verification_embeddings.size(1)))

        sim_matrix = new_get_cossim(verification_embeddings, enrollment_centroids)

        return sim_matrix[0][0][0]

def new_get_cossim(embeddings, centroids):
    # Calculates cosine similarity matrix. Requires (N, M, feature) input
    cossim = torch.zeros(embeddings.size(0),embeddings.size(1),centroids.size(0))
    for speaker_num, speaker in enumerate(embeddings):
        for utterance_num, utterance in enumerate(speaker):
            for centroid_num, centroid in enumerate(centroids):
                output = F.cosine_similarity(utterance,centroid,dim=0)+1e-6
                cossim[speaker_num][utterance_num][centroid_num] = output
    return cossim

if __name__=="__main__":
    predict("./enrollment_set/taesoo-2/New-1.wav")
