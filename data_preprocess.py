#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import glob
import os
import librosa
import numpy as np

class HParam:
    tisv_frame = 180
    hop = 0.01
    window = 0.025
    sr = 16000
    nmels = 40
    nfft = 512

# downloaded dataset path

def save_spectrogram_tisv():
    """ Full preprocess of text independent utterance. The log-mel-spectrogram is saved as numpy file.
        Each partial utterance is splitted by voice detection using DB
        and the first and the last 180 frames from each partial utterance are saved.
        Need : utterance data set (VTCK)
    """
    print("start text independent utterance feature extraction")
    os.makedirs('./predict_tisv', exist_ok=True)    # make folder to save test file
    audio_path = glob.glob(os.path.dirname('./enrollment_set/taesoo/*.wav'))

    utter_min_len = (HParam.tisv_frame * HParam.hop + HParam.window) * HParam.sr    # lower bound of utterance length
    total_speaker_num = len(audio_path)
    for i, folder in enumerate(audio_path):
        utterances_spec = []
        for utter_name in os.listdir(folder):
            if utter_name[-4:] == '.wav':
                utter_path = os.path.join(folder, utter_name)         # path of each utterance
                utter, sr = librosa.core.load(utter_path, HParam.sr)        # load utterance audio
                intervals = librosa.effects.split(utter, top_db=30)         # voice activity detection
                for interval in intervals:
                    if (interval[1]-interval[0]) > utter_min_len:           # If partial utterance is sufficient long,
                        utter_part = utter[interval[0]:interval[1]]         # save first and last 180 frames of spectrogram.
                        S = librosa.core.stft(y=utter_part, n_fft=HParam.nfft,
                                              win_length=int(HParam.window * sr), hop_length=int(HParam.hop * sr))
                        S = np.abs(S) ** 2
                        mel_basis = librosa.filters.mel(sr=HParam.sr, n_fft=HParam.nfft, n_mels=HParam.nmels)
                        S = np.log10(np.dot(mel_basis, S) + 1e-6)           # log mel spectrogram of utterances
                        utterances_spec.append(S[:, :HParam.tisv_frame])    # first 180 frames of partial utterance
                        utterances_spec.append(S[:, -HParam.tisv_frame:])   # last 180 frames of partial utterance

        utterances_spec = np.array(utterances_spec)
        np.save(os.path.join('./predict_tisv', "speaker_0.npy"), utterances_spec)


def process_input(path):
    utter_min_len = (HParam.tisv_frame * HParam.hop + HParam.window) * HParam.sr    # lower bound of utterance length
    utterances_spec = []
    utter, sr = librosa.core.load(path, HParam.sr)        # load utterance audio
    intervals = librosa.effects.split(utter, top_db=30)         # voice activity detection
    for interval in intervals:
        # print('interval', interval, utter_min_len)
        if (interval[1]-interval[0]) > utter_min_len:           # If partial utterance is sufficient long,
            # print('enough long')
            utter_part = utter[interval[0]:interval[1]]         # save first and last 180 frames of spectrogram.
            S = librosa.core.stft(y=utter_part, n_fft=HParam.nfft,
                                  win_length=int(HParam.window * sr), hop_length=int(HParam.hop * sr))
            S = np.abs(S) ** 2
            mel_basis = librosa.filters.mel(sr=HParam.sr, n_fft=HParam.nfft, n_mels=HParam.nmels)
            S = np.log10(np.dot(mel_basis, S) + 1e-6)           # log mel spectrogram of utterances
            utterances_spec.append(S[:, :HParam.tisv_frame])    # first 180 frames of partial utterance
            utterances_spec.append(S[:, -HParam.tisv_frame:])   # last 180 frames of partial utterance

    utterances_spec = np.array(utterances_spec)
    np.save(os.path.join('./predict_tisv', "speaker_0.npy"), utterances_spec)


if __name__ == "__main__":
    save_spectrogram_tisv()
