#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import glob
import numpy as np
import os
import random
from random import shuffle
import torch
from torch.utils.data import Dataset

class HParam:
    tisv_frame = 180
    hop = 0.01
    window = 0.025
    sr = 16000
    nmels = 40
    nfft = 512

def mfccs_and_spec(wav_file, wav_process = False, calc_mfccs=False, calc_mag_db=False):
    sound_file, _ = librosa.core.load(wav_file, sr=HParam.sr)
    window_length = int(HParam.window*HParam.sr)
    hop_length = int(HParam.hop*HParam.sr)
    duration = HParam.tisv_frame * HParam.hop + HParam.window

    # Cut silence and fix length
    if wav_process == True:
        sound_file, index = librosa.effects.trim(sound_file, frame_length=window_length, hop_length=hop_length)
        length = int(HParam.sr * duration)
        sound_file = librosa.util.fix_length(sound_file, length)

    spec = librosa.stft(sound_file, n_fft=HParam.nfft, hop_length=hop_length, win_length=window_length)
    mag_spec = np.abs(spec)

    mel_basis = librosa.filters.mel(HParam.sr, HParam.nfft, n_mels=HParam.nmels)
    mel_spec = np.dot(mel_basis, mag_spec)

    mag_db = librosa.amplitude_to_db(mag_spec)
    #db mel spectrogram
    mel_db = librosa.amplitude_to_db(mel_spec).T

    mfccs = None
    if calc_mfccs:
        mfccs = np.dot(librosa.filters.dct(40, mel_db.shape[0]), mel_db).T

    return mfccs, mel_db, mag_db

class SpeakerDatasetPredictPreprocessed(Dataset):

    def __init__(self, utter_start=0):

        # data path
        self.path = './predict_tisv'
        self.utter_num = 5
        self.file_list = os.listdir(self.path)
        self.utter_start = utter_start

    def __len__(self):
        return len(self.file_list)

    def __getitem__(self, idx):

        np_file_list = os.listdir(self.path)

        selected_file = np_file_list[idx]
        print(os.path.join(self.path, selected_file))
        utters = np.load(os.path.join(self.path, selected_file))        # load utterance spectrogram of selected speaker
        utterance = utters[self.utter_start: self.utter_start+self.utter_num] # utterances of a speaker [batch(M), n_mels, frames]

        print(utterance.shape)
        utterance = utterance[:,:,:160]               # TODO implement variable length batch size

        utterance = torch.tensor(np.transpose(utterance, axes=(0,2,1)))     # transpose [batch, frames, n_mels]
        return utterance
