import verifier
from speech_embedder_net import get_centroids
from data_preprocess import process_input
from data_preprocess import HParam
import pickle
import glob
import os
import librosa
import numpy as np
from speech_embedder_net import SpeechEmbedder
import torch
import os
import random
import time
import torch
import pickle
from torch.utils.data import DataLoader
import torch.nn.functional as F
from data_preprocess import process_input

from data_load import SpeakerDatasetPredictPreprocessed
from speech_embedder_net import SpeechEmbedder, GE2ELoss, get_centroids, get_cossim

def load_utt(path):
    utter_min_len = (HParam.tisv_frame * HParam.hop + HParam.window) * HParam.sr    # lower bound of utterance length
    utterances_spec = []
    utter, sr = librosa.core.load(path, HParam.sr)
    return utter

def load_model():
    embedder_net = SpeechEmbedder()
    embedder_net.load_state_dict(torch.load('./mdl.model'))
    embedder_net.eval()
    return embedder_net

def prepare_audio_for_centroids(path):
    processed_b = process_input(b)
    predict_dataset = SpeakerDatasetPredictPreprocessed()
    for el in predict_dataset:
        return el
    # test_loader = DataLoader(predict_dataset, batch_size=1, num_workers=8, drop_last=True)

    #for el in test_loader:
    #    return el

def are_similar(a, b, verbose=False):
    processed_b = prepare_audio_for_centroids(b)
    if verbose:
        print('Processed', processed_b.shape)
    model = load_model()
    if verbose:
        print('model', model)
    embedd_b = model(processed_b)
    centroids_b = get_centroids(embedd_b)
    if verbose:
        print('Centroids shape', centroids_b.shape)
    if verbose:
        with open('enrollment_centroids.pkl', 'rb') as file:
            print(pickle.load(file).shape)
    with open('mycentroids.pkl', 'wb') as file:
        pickle.dump(centroids_b, file)
    return verifier.predict(a, 'mycentroids.pkl').item()

files = [el for el in os.listdir('.') if '.ogg' in el]

pred = []
truth = []

for i in range(len(files)):
    for j in range(i):
        a = files[i]
        b = files[j]
        pred.append((are_similar(a, b) + are_similar(b, a)) / 2)
        if a[:3] == b[:3]:
            print(a, b)
            truth.append(1)
        else:
            truth.append(0)

thr0 = 0.965 # pred[0]
best_acc = 0

for i in range(len(pred)):
    if pred[i] < thr:
        good += truth[i] == 0
    else:
        good += truth[i] == 1
best_acc = good

print(best_acc, best_acc / len(pred))
print('thr', thr)
exit(0)
